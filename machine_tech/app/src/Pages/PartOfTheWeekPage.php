<?php

use SilverStripe\Forms\TextField;
use SilverStripe\Forms\GridField\GridField;
use Tedy\GridFieldCustom\GridFieldDeleteAllButton;
use SilverStripe\Forms\GridField\GridFieldPageCount;
use SilverStripe\Forms\GridField\GridFieldPaginator;
use Tedy\GridFieldCustom\GridFieldMultiDeleteButton;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;
use Tedy\GridFieldCustom\GridFieldCheckboxSelectComponent;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;

class PartOfTheWeekPage extends Page
{
  private static $icon_class = 'font-icon-clock';

  private static $has_many = [
    "PartOfTheWeekGalleries" => PartOfTheWeekGallery::class
  ];

  /**
   * CMS Fields
   * @return FieldList
   */
  public function getCMSFields()
  {
    $fields = parent::getCMSFields();

    $configPartOfTheWeekGalleries = GridFieldConfig_RecordEditor::create();
    $configPartOfTheWeekGalleries->addComponent(new GridFieldOrderableRows());
    $configPartOfTheWeekGalleries->addComponent(
      new GridFieldCheckboxSelectComponent()
    );
    // buttons-before-right
    // -position right side before the search, it will not hide the buttons upon search
    $configPartOfTheWeekGalleries->addComponent(
      new GridFieldMultiDeleteButton('before')
    );
    $configPartOfTheWeekGalleries->addComponent(
      new GridFieldDeleteAllButton('before')
    );

    $fields->addFieldsToTab(
      'Root.Main',
      [
        $PartOfTheWeekGalleries = GridField::create('PartOfTheWeekGalleries', '', $this->PartOfTheWeekGalleries(), $configPartOfTheWeekGalleries),
      ],
      "Content"
    );

    $configPartOfTheWeekGalleries->removeComponentsByType(GridFieldPaginator::class);
    $configPartOfTheWeekGalleries->removeComponentsByType(GridFieldPageCount::class);

    return $fields;
  }
}
