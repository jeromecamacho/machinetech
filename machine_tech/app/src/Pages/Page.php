<?php

namespace {

    use SilverStripe\Forms\HTMLEditor\TinyMCEConfig;


    use SilverStripe\Forms\HeaderField;
    use SilverStripe\Forms\LiteralField;
    use SilverStripe\Forms\TextareaField;
    use Sheadawson\Linkable\Forms\LinkField;
    use UncleCheese\DisplayLogic\Forms\Wrapper;
    use SilverStripe\Forms\HTMLEditor\HTMLEditorField;

    use SilverStripe\Forms\CheckboxField;
    use SilverStripe\Forms\OptionsetField;

    use Sheadawson\Linkable\Models\Link;

    use SilverStripe\AssetAdmin\Forms\UploadField;

    use SilverStripe\Forms\TextField;

    use SilverStripe\Forms\Tab;
    use SilverStripe\Forms\TabSet;
    use SilverStripe\Forms\ToggleCompositeField;

    use SilverStripe\Assets\Image;
    use SilverStripe\CMS\Model\SiteTree;
    use SilverStripe\Forms\ListboxField;
    use SilverStripe\Forms\TreeMultiselectField;

    class Page extends SiteTree
    {
        private static $db = [
            "PageLabel" => "Text",

            //cta
            "UseOwnCTA" => "Boolean",
            "IsHaveCTA" => "Boolean",
            "CTAStyle" => "Enum('Style1,Style2,Custom')",

            // style 1
            "CTAStyle1Label" => "Text",
            "CTAStyle1Description" => "Text",

            // style 2
            "CTAStyle2LeftSideLabel" => "Text",
            "CTAStyle2LeftSideDescription" => "Text",
            "CTAStyle2RightSideLabel" => "Text",
            "CTAStyle2RightSideDescription" => "Text",
            "CTAStyle2BottomAreaLabel" => "Text",

            // custom
            "CTACustom" => "HTMLText",

            //additional metatags
            'MetaTitle' => 'Text',
            'MetaKeywords' => 'Text',

            // featured services page
            'IsFeaturedServicesPages' => "Boolean(1)",
            "TitleFeaturedServicesPages" => "Text",

            "IsCTAWhoWeAreType" => "Boolean",
            "CTAWhoWeAreType" => "Enum('Style1,Custom')",
            "CTAWhoWeAreLabel" => "Text",
            "CTAWhoWeAreDescription" => "Text",
            "CTAWhoWeAreCustom" => "HTMLText"
        ];

        private static $has_one = [
            "BannerBackground" => Image::class,

            // style 1
            "CTAStyle1Link" => Link::class,
            "CTAStyle1Background" => Image::class,

            // style 2
            "CTAStyle2LeftSideLink" => Link::class,
            "CTAStyle2RightSideLink" => Link::class,
            "CTAStyle2BottomAreaLink" => Link::class,
            "CTAStyle2Background" => Image::class,

            // featured services page
            "FeaturedServicesPagesBackground" => Image::class,

            "CTAWhoWeAreLink" => Link::class,
            "CTAWhoWeAreBackground" => Image::class
        ];

        private static $many_many = [
            "ServicesPages" => ServicesPage::class
        ];

        /**
         * CMS Fields
         * @return FieldList
         */
        public function getCMSFields()
        {
            $fields = parent::getCMSFields();

            // reposition Metadata
            if ($metaData = $fields->fieldByName('Root.Main.Metadata')) {
                $fields->removeFieldFromTab('Root.Main', 'Metadata');
                $metaData->setTitle("Meta Tags Configuration");
                $fields->fieldByName('Root.Main')->unshift($metaData);
            }
            $fields->addFieldsToTab(
                'Root.Main',
                [
                    TextField::create(
                        'MetaTitle',
                        'Meta Title'
                    ),
                    TextareaField::create(
                        'MetaKeywords',
                        'Meta Keywords'
                    ),
                ],
                "MetaDescription"
            );
            // end of meta data

            $fields->addFieldsToTab(
                "Root.Main",
                [
                    ToggleCompositeField::create(
                        'Banner',
                        'Banner',
                        array(
                            new TabSet(
                                'BannerTabSet',
                                new Tab(
                                    'Main Content',
                                    TextField::create('PageLabel', 'Page Label')->setRightTitle("If have value, it will prioritize to view as the Main Label of the Page "),
                                    $BannerBackground = UploadField::create('BannerBackground', "Background")->setRightTitle("If have value, it will prioritize this Background")
                                )
                            )
                        )
                    )->setHeadingLevel(4)->setStartClosed(true),
                    ToggleCompositeField::create(
                        'CTA - Who We Are',
                        'CTA - Who We Are',
                        array(
                            CheckboxField::create(
                                'IsCTAWhoWeAreType',
                                'Show ?'
                            ),
                            $CTAWhoWeAreType = new OptionsetField("CTAWhoWeAreType", "CTA Style/Type",  singleton(AboutPage::class)
                                ->dbObject("CTAWhoWeAreType")
                                ->enumValues()),

                            // cta style1
                            $CTAWhoWeAreLabel = TextField::create(
                                'CTAWhoWeAreLabel',
                                'Label'
                            ),
                            $CTAWhoWeAreDescription = TextareaField::create(
                                'CTAWhoWeAreDescription',
                                'Description'
                            ),
                            $CTAWhoWeAreLink = LinkField::create(
                                'CTAWhoWeAreLinkID',
                                'Link'
                            ),
                            $CTAWhoWeAreBackground = UploadField::create('CTAWhoWeAreBackground', 'Background Image'),

                            //  cta custom
                            $CTAWhoWeAreCustom = HtmlEditorField::create(
                                'CTAWhoWeAreCustom',
                                'Custom CTA'
                            )->setRows(10)

                        )
                    ),
                    ToggleCompositeField::create(
                        'Featured Services Pages',
                        'Featured Services Pages',
                        array(
                            new TabSet(
                                'FeaturedServicesPagesTabSet',
                                new Tab(
                                    'Main Content',
                                    CheckboxField::create("IsFeaturedServicesPages", "Show Featured Services Page?"),
                                    TextField::create("TitleFeaturedServicesPages", "Label")->setRightTitle("If have value, it will prioritize to view as the Main Label"),
                                    $FeaturedServicesPagesBackground = UploadField::create('FeaturedServicesPagesBackground', "Background")->setRightTitle("If have value, it will prioritize this Background")
                                )
                            )
                        )
                    )->setHeadingLevel(4)->setStartClosed(true),

                    CheckboxField::create("UseOwnCTA", "Use Own CTA"),
                    ToggleCompositeField::create(
                        'CTA',
                        'CTA',
                        array(
                            CheckboxField::create("IsHaveCTA", "Show CTA?"),
                            $CTAStyle = new OptionsetField("CTAStyle", "CTA Style/Type",  singleton(Page::class)
                                ->dbObject("CTAStyle")
                                ->enumValues()),
                            $CTAPreviewCaption = Wrapper::create(HeaderField::create(
                                'CTAPreviewCaption',
                                'Sample/Preview:',
                                3
                            )),
                            $CTAStyle1Preview = Wrapper::create(LiteralField::create('CTAStyle1Preview', "<img src = 'app/images/cta_style1.png' width = '450' height = '200'>")),
                            $CTAStyle2Preview = Wrapper::create(LiteralField::create('CTAStyle2Preview', "<img src = 'app/images/cta_style2.png' width = '450' height = '200'>")),

                            $CTASeparatorPreviewing = LiteralField::create('CTASeparatorPreviewing', "</br></br>"),

                            // cta style1
                            $CTAStyle1Label = TextField::create(
                                'CTAStyle1Label',
                                'Label'
                            ),
                            $CTAStyle1Description = TextareaField::create(
                                'CTAStyle1Description',
                                'Description'
                            ),
                            $CTAStyle1Link = LinkField::create(
                                'CTAStyle1LinkID',
                                'Link'
                            ),
                            $CTAStyle1Background = UploadField::create('CTAStyle1Background', 'Background Image'),

                            // cta style2
                            $Style2Tabset = Wrapper::create(new TabSet(
                                'CTATabSetStyle2',
                                new Tab(
                                    'Background',
                                    $CTAStyle2Background = UploadField::create('CTAStyle2Background', 'Background Image'),
                                ),
                                new Tab(
                                    'Left Side',
                                    $CTAStyle2LeftSideLabel =
                                        TextField::create(
                                            'CTAStyle2LeftSideLabel',
                                            'Label'
                                        ),
                                    $CTAStyle2LeftSideDescription = TextareaField::create(
                                        'CTAStyle2LeftSideDescription',
                                        'Description'
                                    ),
                                    $CTAStyle2LeftSideLink = LinkField::create(
                                        'CTAStyle2LeftSideLinkID',
                                        'Link'
                                    )
                                ),
                                new Tab(
                                    'Right Side',
                                    $CTAStyle2RightSideLabel =
                                        TextField::create(
                                            'CTAStyle2RightSideLabel',
                                            'Label'
                                        ),
                                    $CTAStyle2RightSideDescription = TextareaField::create(
                                        'CTAStyle2RightSideDescription',
                                        'Description'
                                    ),
                                    $CTAStyle2RightSideLink = LinkField::create(
                                        'CTAStyle2RightSideLinkID',
                                        'Link'
                                    ),
                                ),
                                new Tab(
                                    'Bottom Area',
                                    $CTAStyle2BottomAreaLabel = TextareaField::create(
                                        'CTAStyle2BottomAreaLabel',
                                        'Description'
                                    ),
                                    $CTAStyle2RightSideLink = LinkField::create(
                                        'CTAStyle2BottomAreaLinkID',
                                        'Link'
                                    ),
                                )
                            )),

                            //  cta custom
                            $CTACustom = HTMLEditorField::create(
                                'CTACustom',
                                'Custom CTA'
                            )->setRows(10)
                            // ->setEditorConfig(TinyMCEConfig::get('home_hero'))

                        )
                    )

                    // ToggleCompositeField::create(
                    //     'Featured Services Page',
                    //     'Featured Services Page',
                    //     array(
                    // TreeMultiselectField::create(
                    //     "ServicesPages",
                    //     "Services Page",
                    //     ServicesPage::class
                    // )
                    //     ->setDisableFunction(
                    //         function ($item) {
                    //             return $item->Classname !== ServicesPage::class;
                    //         }
                    //     ),

                    // ListboxField::create(
                    //     "ServicesPages",
                    //     "Featured Services Pages",
                    //     ServicesPage::get()->map("ID", "Title")->toArray()
                    // ),

                    // LiteralField::create("FeaturedServicesPageBreak", "<br><br><br><br><br><br><br><br><br><br>")
                    // )
                    // )
                ],
                "Content"
            );

            $BannerBackground->setAllowedExtensions(['png', 'gif', 'jpg', 'jpeg']);
            $BannerBackground->setFolderName('Banner-Background-Photos');


            // cta who we are
            $CTAWhoWeAreBackground->setAllowedExtensions(['png', 'gif', 'jpg', 'jpeg']);
            $CTAWhoWeAreBackground->setFolderName('CTA-WhoWeAre-Background-Photos');

            $CTAWhoWeAreLabel->displayIf("CTAWhoWeAreType")->isEqualTo("Style1");
            $CTAWhoWeAreDescription->displayIf("CTAWhoWeAreType")->isEqualTo("Style1");
            $CTAWhoWeAreLink->displayIf("CTAWhoWeAreType")->isEqualTo("Style1");
            $CTAWhoWeAreBackground->displayIf("CTAWhoWeAreType")->isEqualTo("Style1");

            $CTAWhoWeAreCustom->displayIf("CTAWhoWeAreType")->isEqualTo("Custom");


            $FeaturedServicesPagesBackground->setAllowedExtensions(['png', 'gif', 'jpg', 'jpeg']);
            $FeaturedServicesPagesBackground->setFolderName('Featured-ServicesPages-Photos');


            // cta
            $CTAStyle1Label->displayIf("CTAStyle")->isEqualTo("Style1");
            $CTAStyle1Description->displayIf("CTAStyle")->isEqualTo("Style1");
            $CTAStyle1Link->displayIf("CTAStyle")->isEqualTo("Style1");
            $CTAStyle1Background->displayIf("CTAStyle")->isEqualTo("Style1");
            $CTAStyle1Preview->displayIf("CTAStyle")->isEqualTo("Style1");

            $Style2Tabset->displayIf("CTAStyle")->isEqualTo("Style2");
            $CTAStyle2Preview->displayIf("CTAStyle")->isEqualTo("Style2");

            $CTAPreviewCaption->displayIf("CTAStyle")->isEqualTo("Style1")->orIf("CTAStyle")->isEqualTo("Style2");

            $CTACustom->displayIf("CTAStyle")->isEqualTo("Custom");

            $CTAStyle1Background->setAllowedExtensions(['png', 'gif', 'jpg', 'jpeg']);
            $CTAStyle1Background->setFolderName('CTA-Photos');

            $CTAStyle2Background->setAllowedExtensions(['png', 'gif', 'jpg', 'jpeg']);
            $CTAStyle2Background->setFolderName('CTA-Photos');

            return $fields;
        }
    }
}
