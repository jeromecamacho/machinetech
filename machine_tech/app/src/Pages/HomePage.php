<?php

class HomePage extends Page
{
  private static $icon_class = 'font-icon-home';

  /**
   * CMS Fields
   * @return FieldList
   */
  public function getCMSFields()
  {
    $fields = parent::getCMSFields();
    $fields->removeByName([
      'Banner',
      'Featured Services Pages',
      'CTA - Who We Are'
    ]);
    return $fields;
  }
}
