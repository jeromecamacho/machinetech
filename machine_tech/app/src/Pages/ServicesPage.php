<?php

use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\File;

class ServicesPage extends Page
{
  private static $icon_class = 'font-icon-rocket';

  private static $belongs_many_many = [
    "Pages" => Page::class
  ];

  private static $has_one = [
    "Icon" => File::class
  ];

  /**
   * CMS Fields
   * @return FieldList
   */
  public function getCMSFields()
  {
    $fields = parent::getCMSFields();
    $fields->addFieldsToTab(
      'Root.Main',
      [
        UploadField::create(
          'Icon'
        )
      ],
      "Content"
    );
    return $fields;
  }

  public function getBreakTitleWithBr()
  {
    if ($this->Title) {

      $replacedString = str_replace('-', ' ', $this->Title); // remove all hypens.

      $string = preg_replace('/[^A-Za-z0-9\-]/', ' ', $replacedString); // Removes special chars.

      if (str_word_count($string) == 2) {

        //set only 1 space
        $string =
          preg_replace('!\s+!', ' ', $string);

        $string = str_replace(" ", "\n", $string);
      }

      return $string;
    } else {
      return "";
    }
  }
}
