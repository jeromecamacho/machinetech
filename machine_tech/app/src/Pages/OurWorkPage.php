<?php

use SilverStripe\Forms\GridField\GridField;
use Tedy\GridFieldCustom\GridFieldDeleteAllButton;
use SilverStripe\Forms\GridField\GridFieldPageCount;
use SilverStripe\Forms\GridField\GridFieldPaginator;
use Tedy\GridFieldCustom\GridFieldMultiDeleteButton;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;
use Tedy\GridFieldCustom\GridFieldCheckboxSelectComponent;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;

class OurWorkPage extends Page
{
  private static $icon_class = 'font-icon-block-file-list';

  private static $many_many = [
    "OurWorkGalleries"  => OurWorkGallery::class
  ];

  /**
   * CMS Fields
   * @return FieldList
   */
  public function getCMSFields()
  {
    $fields = parent::getCMSFields();

    $configOurWorkGalleries = GridFieldConfig_RecordEditor::create();
    $configOurWorkGalleries->addComponent(new GridFieldOrderableRows());
    $configOurWorkGalleries->addComponent(
      new GridFieldCheckboxSelectComponent()
    );
    // buttons-before-right
    // -position right side before the search, it will not hide the buttons upon search
    $configOurWorkGalleries->addComponent(
      new GridFieldMultiDeleteButton('before')
    );
    $configOurWorkGalleries->addComponent(
      new GridFieldDeleteAllButton('before')
    );

    $fields->addFieldsToTab(
      'Root.Main',
      [
        $OurWorkGalleries = GridField::create('OurWorkGalleries', '', $this->OurWorkGalleries(), $configOurWorkGalleries),
      ],
      "Content"
    );

    $configOurWorkGalleries->removeComponentsByType(GridFieldPaginator::class);
    $configOurWorkGalleries->removeComponentsByType(GridFieldPageCount::class);

    return $fields;
  }
}
