<?php

use SilverStripe\Assets\Image;
use SilverStripe\Forms\TextField;
use Sheadawson\Linkable\Models\Link;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\OptionsetField;
use Sheadawson\Linkable\Forms\LinkField;
use SilverStripe\Forms\ToggleCompositeField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SwiftDevLabs\CodeEditorField\Forms\CodeEditorField;

class AboutPage extends Page
{
  private static $icon_class = 'font-icon-torsos-all';

  private static $db = [];

  private static $has_one = [];

  /**
   * CMS Fields
   * @return FieldList
   */
  public function getCMSFields()
  {
    $fields = parent::getCMSFields();

    return $fields;
  }
}
