<?php


use SilverStripe\ORM\DataExtension;

class FileExtension extends DataExtension
{
  /**
   * Automatically publish all files when written
   * 
   * @return null
   */
  public function onAfterWrite()
  {
    $this->getOwner()->publishRecursive();
  }
}
