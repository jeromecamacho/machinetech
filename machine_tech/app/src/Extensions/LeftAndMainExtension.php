<?php

use SilverStripe\Core\Extension;
use SilverStripe\View\Requirements;
use SilverStripe\SiteConfig\SiteConfig;

class LeftAndMainExtension extends Extension
{
  public function init()
  {
    $getSiteConfig = SiteConfig::current_site_config();
    $getSiteConfigTitle = $getSiteConfig->Title;

    $getSiteConfigLogo = $getSiteConfig->NavLogo()->URL;

    if ($getSiteConfigLogo) {
      Requirements::customScript(
        <<<JS
          // document.querySelector("#cms-menu > .cms-menu__header > .cms-sitename").innerHTML = "<img src = 'app/images/all4-frisco-logo.svg' height = '25px' width = '70px'>"+ "$getSiteConfigTitle";

          document.querySelector("#cms-menu > .cms-menu__header > .cms-sitename").innerHTML = "<img src = '$getSiteConfigLogo' width = '90px' height = '38px;'>";
        JS
      );
    }
  }
}
