<?php

use SilverStripe\Forms\Tab;
use SilverStripe\Assets\File;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\TabSet;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\HeaderField;
use SilverStripe\ORM\DataExtension;
use Sheadawson\Linkable\Models\Link;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\OptionsetField;
use SilverStripe\SiteConfig\SiteConfig;
use Sheadawson\Linkable\Forms\LinkField;
use UncleCheese\DisplayLogic\Forms\Wrapper;
use SilverStripe\Forms\ToggleCompositeField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;

class SiteConfigExtension extends DataExtension
{
  private static $db = [
    "Copyright" => "Text",
    "TeaserLabel1" => "Text",
    "TeaserLabel2" => "Text",

    "Email" => "Text",
    "Phone" => "Text",

    // global cta
    "GlobalIsHaveCTA" => "Boolean",
    "GlobalCTAStyle" => "Enum('Style1,Style2,Custom')",

    //global style 1
    "GlobalCTAStyle1Label" => "Text",
    "GlobalCTAStyle1Description" => "Text",

    //global style 2
    "GlobalCTAStyle2LeftSideLabel" => "Text",
    "GlobalCTAStyle2LeftSideDescription" => "Text",
    "GlobalCTAStyle2RightSideLabel" => "Text",
    "GlobalCTAStyle2RightSideDescription" => "Text",
    "GlobalCTAStyle2BottomAreaLabel" => "Text",

    //global custom
    "GlobalCTACustom" => "HTMLText"


  ];

  private static $has_one = [
    "NavLogo" => File::class,
    "FaviconIcon" => Image::class,
    "FooterLogo" => File::class,

    "FreeQuoteLink" => Link::class,

    "Location" => Link::class,
    "FacebookLink" => Link::class,

    //global style 1
    "GlobalCTAStyle1Link" => Link::class,
    "GlobalCTAStyle1Background" => Image::class,

    //global style 2
    "GlobalCTAStyle2LeftSideLink" => Link::class,
    "GlobalCTAStyle2RightSideLink" => Link::class,
    "GlobalCTAStyle2BottomAreaLink" => Link::class,
    "GlobalCTAStyle2Background" => Image::class,

  ];

  /**
   * Update Fields
   * @return FieldList
   */
  public function updateCMSFields(FieldList $fields)
  {
    $fields->addFieldsToTab(
      'Root.Main',
      [
        ToggleCompositeField::create(
          'Header',
          'Header',
          array(
            new TabSet(
              'HeaderTabSet',
              new Tab(
                'Logo',
                $navLogo = UploadField::create('NavLogo', "Logo")
              ),
              new Tab(
                'Favicon',
                $faviconIcon = UploadField::create('FaviconIcon', 'Icon')
              ),
              new Tab(
                'Other',
                LinkField::create(
                  'FreeQuoteLinkID',
                  "Free Quote Link"
                )
              )
            )
          )
        ),

        ToggleCompositeField::create(
          'Global CTA',
          'Global CTA',
          array(
            CheckboxField::create("GlobalIsHaveCTA", "Show CTA?"),
            $GlobalCTAStyle = new OptionsetField("GlobalCTAStyle", "CTA Style/Type",  singleton(SiteConfig::class)
              ->dbObject("GlobalCTAStyle")
              ->enumValues()),
            $CTAPreviewCaption = Wrapper::create(HeaderField::create(
              'CTAPreviewCaption',
              'Sample/Preview:',
              3
            )),
            $CTAStyle1Preview = Wrapper::create(LiteralField::create('CTAStyle1Preview', "<img src = 'app/images/cta_style1.png' width = '450' height = '200'>")),
            $CTAStyle2Preview = Wrapper::create(LiteralField::create('CTAStyle2Preview', "<img src = 'app/images/cta_style2.png' width = '450' height = '200'>")),

            $CTASeparatorPreviewing = LiteralField::create('CTASeparatorPreviewing', "</br></br>"),

            //global cta style1
            $GlobalCTAStyle1Label = TextField::create(
              'GlobalCTAStyle1Label',
              'Label'
            ),
            $GlobalCTAStyle1Description = TextareaField::create(
              'GlobalCTAStyle1Description',
              'Description'
            ),
            $GlobalCTAStyle1Link = LinkField::create(
              'GlobalCTAStyle1LinkID',
              'Link'
            ),
            $GlobalCTAStyle1Background = UploadField::create('GlobalCTAStyle1Background', 'Background Image'),

            //global cta style2
            $GlobalStyle2Tabset = Wrapper::create(new TabSet(
              'GlobalCTATabSetStyle2',
              new Tab(
                'Background',
                $GlobalCTAStyle2Background = UploadField::create('GlobalCTAStyle2Background', 'Background Image'),
              ),
              new Tab(
                'Left Side',
                $GlobalCTAStyle2LeftSideLabel =
                  TextField::create(
                    'GlobalCTAStyle2LeftSideLabel',
                    'Label'
                  ),
                $GlobalCTAStyle2LeftSideDescription = TextareaField::create(
                  'GlobalCTAStyle2LeftSideDescription',
                  'Description'
                ),
                $GlobalCTAStyle2LeftSideLink = LinkField::create(
                  'GlobalCTAStyle2LeftSideLinkID',
                  'Link'
                )
              ),
              new Tab(
                'Right Side',
                $GlobalCTAStyle2RightSideLabel =
                  TextField::create(
                    'GlobalCTAStyle2RightSideLabel',
                    'Label'
                  ),
                $GlobalCTAStyle2RightSideDescription = TextareaField::create(
                  'GlobalCTAStyle2RightSideDescription',
                  'Description'
                ),
                $GlobalCTAStyle2RightSideLink = LinkField::create(
                  'GlobalCTAStyle2RightSideLinkID',
                  'Link'
                ),
              ),
              new Tab(
                'Bottom Area',
                $GlobalCTAStyle2BottomAreaLabel = TextareaField::create(
                  'GlobalCTAStyle2BottomAreaLabel',
                  'Description'
                ),
                $GlobalCTAStyle2RightSideLink = LinkField::create(
                  'GlobalCTAStyle2BottomAreaLinkID',
                  'Link'
                ),
              )
            )),

            // global cta custom
            $GlobalCTACustom = HTMLEditorField::create(
              'GlobalCTACustom',
              'Custom CTA'
            )->setRows(10)

          )
        ),

        ToggleCompositeField::create(
          'Footer',
          'Footer',
          array(
            new TabSet(
              'FooterTabSet',
              new Tab(
                'Logo',
                $footerLogo = UploadField::create('FooterLogo', "Logo")
              ),
              new Tab(
                'Other',
                TextField::create('Copyright', 'Copyright'),
                TextField::create('TeaserLabel1', 'Teaser Label 1'),
                TextField::create('TeaserLabel2', 'Teaser Label 2'),
              )
            )
          )
        ),
        ToggleCompositeField::create(
          'Contact Details',
          'Contact Details',
          array(
            new TabSet(
              'ContactDetailsTabSet',
              new Tab(
                'Contact Details',
                LinkField::create(
                  'LocationID',
                  'Location',
                ),
                TextField::create('Phone'),
                TextField::create('Email'),
                LinkField::create('FacebookLinkID', 'Facebook Link'),
              )
            )
          )
        ),

      ]
    );

    $navLogo->setAllowedExtensions(['svg', 'png', 'gif', 'jpg', 'jpeg']);
    $navLogo->setFolderName('Navigation-Logo');

    $faviconIcon->setAllowedExtensions(['png', 'gif', 'jpg', 'jpeg']);
    $faviconIcon->setFolderName('Favicon-Icon');

    $footerLogo->setAllowedExtensions(['svg', 'png', 'gif', 'jpg', 'jpeg']);
    $footerLogo->setFolderName('Footer-Logo');

    $GlobalCTAStyle1Label->displayIf("GlobalCTAStyle")->isEqualTo("Style1");
    $GlobalCTAStyle1Description->displayIf("GlobalCTAStyle")->isEqualTo("Style1");
    $GlobalCTAStyle1Link->displayIf("GlobalCTAStyle")->isEqualTo("Style1");
    $GlobalCTAStyle1Background->displayIf("GlobalCTAStyle")->isEqualTo("Style1");
    $CTAStyle1Preview->displayIf("GlobalCTAStyle")->isEqualTo("Style1");

    $GlobalStyle2Tabset->displayIf("GlobalCTAStyle")->isEqualTo("Style2");
    $CTAStyle2Preview->displayIf("GlobalCTAStyle")->isEqualTo("Style2");

    $CTAPreviewCaption->displayIf("GlobalCTAStyle")->isEqualTo("Style1")->orIf("GlobalCTAStyle")->isEqualTo("Style2");

    $GlobalCTACustom->displayIf("GlobalCTAStyle")->isEqualTo("Custom");

    $GlobalCTAStyle1Background->setAllowedExtensions(['png', 'gif', 'jpg', 'jpeg']);
    $GlobalCTAStyle1Background->setFolderName('Global-CTA-Photos');

    $GlobalCTAStyle2Background->setAllowedExtensions(['png', 'gif', 'jpg', 'jpeg']);
    $GlobalCTAStyle2Background->setFolderName('Global-CTA-Photos');

    $fields->removeByName([
      'Tagline'
    ]);
  }
}
