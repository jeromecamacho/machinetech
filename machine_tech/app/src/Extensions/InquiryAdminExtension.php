<?php

use SilverStripe\Core\Extension;
use SilverStripe\Forms\DateField;

class InquiryAdminExtension extends Extension
{

  public function updateSearchContext($context)
  {
    // don't need to wrap field name inside the `q[]` tag anymore
    $dateField = DateField::create('Created', 'Created Date');
    $context->getFields()->push($dateField);
  }
}
