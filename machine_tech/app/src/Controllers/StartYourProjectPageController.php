<?php

use SilverStripe\Assets\File;
use SilverStripe\Assets\Folder;
use SilverStripe\Assets\Upload;
use PHPMailer\PHPMailer\PHPMailer;
use SilverStripe\Control\Director;
use SilverStripe\View\Requirements;
use SilverStripe\Control\HTTPRequest;

class StartYourProjectPageController extends PageController
{

  private static $allowed_actions = [
    "_submitInquiryStartYourProject"
  ];

  private function getLinkAjax()
  {
    return str_replace("?stage=Stage", "", Director::get_current_page()->Link());
  }

  public function init()
  {
    parent::init();

    // ! FORMS SCRIPT
    $linkForAjax = $this->getLinkAjax();
    $cleanBaseHref = $this->getCleanBaseHref();
    $currentLink = $this->Link();

    Requirements::customScript(
      <<<JS
        $(document).ready(function () {

          const validateAll = () => {
            if ($("#idFrmInquiryStartYourProject").length) {
              // custom validation
              $.validator.addMethod("noSpace", (value, element) => {
                return value === '' || value.trim().length != 0
              }, "Spaces are not allowed");

              $.validator.addMethod("phoneValidation", (value, element) => {
                var isValid = true;
                if ($(element).hasClass('has-error')) {
                  isValid = false;
                }
                return isValid;
              }, "Invalid Phone Number");

              $('#idFrmInquiryStartYourProject').validate({
                // render error message corresponds on each fields
                // errorPlacement: function(error, element) {
                //   error.appendTo(element.closest("div"));
                // },

                // one div for all message
                errorContainer: $('div.container-error'),
                errorLabelContainer: $("ul", $('div.container-error')),
                wrapper: 'li',

                // to include the hidden field recaptcha for validation
                ignore: ".ignore",

                rules: {
                  first_name: {
                    required: true,
                    noSpace: true
                  },
                  last_name: {
                    required: true,
                    noSpace: true,
                  },
                  email: {
                    required: true,
                    email: true,
                    noSpace: true,
                  },
                  phone: {
                    required: true,
                    noSpace: true,
                    phoneValidation: true
                  },
                  company_name: {
                    required: true,
                    noSpace: true,
                  },
                  message: {
                    required: true,
                    noSpace: true
                  },
                  hidden_recaptcha: {
                    required: function () {
                      if (grecaptcha.getResponse() == '') {
                        return true;
                      } else {
                        return false;
                      }
                    }
                  }
                },
                // for customize the error message
                messages: {
                  first_name: {
                    required: "Please enter First Name"
                  },
                  last_name: {
                    required: "Please enter Last Name"
                  },
                  email: {
                    required: "Please enter Email Address"
                  },
                  phone: {
                    required: "Please enter Phone Number"
                  },
                  company_name: {
                    required: "Please enter Company Name"
                  },
                  message: {
                    required: "Please enter Message"
                  },
                  hidden_recaptcha: {
                    required: "Please check Recaptcha"
                  }
                }
              });
            }
          }

          validateAll();

          // for phone
          $('#phone').alphanum({
            allowLatin: false,
            allowOtherCharSets: false,
            allow: '+,()- ', // Allow extra characters
          });

          var input = document.querySelector("#phone");

          // initialise plugin
          var iti = window.intlTelInput(input, {
            // get current location of the user
            // initialCountry: "auto",
            // geoIpLookup: function(callback) {
            //     $.get('https://ipinfo.io', function() {}, "jsonp").always(function(resp) {
            //     var countryCode = (resp && resp.country) ? resp.country : "";
            //     callback(countryCode);
            //     });
            // },
            utilsScript: "app/js/utils.js"
          });

          var reset = function () {
            input.classList.remove("has-error");
          };

          // on blur: validate
          input.addEventListener('blur', function () {
            reset();
            if (input.value.trim()) {
              if (!iti.isValidNumber()) {
                input.classList.add("has-error");
              }
            }
          });

          // on keyup / change flag: reset
          input.addEventListener('change', reset);
          input.addEventListener('keyup', reset);
          // end of for phone

          $("#idFrmInquiryStartYourProject").on('submit', function (event) {

            var thisForm = $("#idFrmInquiryStartYourProject");
            var thisSubmitButton = $("#idFrmInquiryStartYourProject button");

            if (!$(thisForm).valid()) {
              return false;
            }
            event.preventDefault();

            var has_phone_error = 0;

            if ($("#phone").hasClass("has-error")) {
              has_phone_error = 1;
            }

            // new ajax
            var formData = new FormData(this);
            formData.append("has_phone_error",has_phone_error);

            $.ajax({
                url: "$linkForAjax"+"_submitInquiryStartYourProject",
                type: 'POST',
                beforeSend: function () {
                  thisSubmitButton.html("<i class = 'fa fa-spinner fa-spin'></i> Please Wait...").attr("disabled", true);
                },
                data: formData,
                success: function (response) {
                  
                  grecaptcha.reset();
                  $("#idFrmInquiryStartYourProject")[0].reset();
                  thisSubmitButton.html('Submit').attr("disabled", false);
                  // console.log(response);

                  var decodeResponse = JSON.parse(response);

                  if (!decodeResponse.is_success) {
                    alert(decodeResponse.message);
                  } else if (decodeResponse.is_success) {
                    window.location = "$currentLink"+"thank-you";
                  } else {
                    alert(response);
                  }

                },
                cache: false,
                contentType: false,
                processData: false
            });
            // end of new ajax


          });

        });
      JS
    );
  }

  public function _submitInquiryStartYourProject(HTTPRequest $request)
  {
    $data = $request->requestVars();

    $firstName = $data['first_name'];
    $lastName = $data['last_name'];
    $name = $firstName . " " . $lastName;

    $email = $data['email'];
    $phone = $data['phone'];
    $companyName = $data['company_name'];
    $message = $data['message'];

    $imageFile = $data['fileUpload'];

    // VALIDATION
    $arr_error = [];

    if (trim($firstName) == "") {
      $arr_error[] = "First Name is required";
    }

    if (trim($lastName) == "") {
      $arr_error[] = "Last Name is required";
    }

    if (trim($email) == "") {
      $arr_error[] = "Email is required";
    } else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $arr_error[] = "Email is Invalid";
    }

    if (trim($phone) == "") {
      $arr_error[] = "Phone Number is required";
    } else if ($data['has_phone_error'] == 1) {
      $arr_error[] = "Invalid Phone Number";
    }

    if (trim($companyName) == "") {
      $arr_error[] = "Company Name is required";
    }

    if (trim($message) == "") {
      $arr_error[] = "Message is required";
    }

    if (!empty($imageFile['tmp_name'])) {
      $file_type = $imageFile['type']; //returns the mimetype
      $allowed = array("image/jpeg", "image/jpg", "image/gif", "image/png");
      if (!in_array($file_type, $allowed)) {
        $arr_error[] = "Only jpg, gif, and png files are allowed.";
      }
    }

    if (trim($data['g-recaptcha-response']) === "") {
      $arr_error[] = "Please check the captcha form";
    } else {
      $captcha_data = array(
        'secret' => "6Leo9n8UAAAAADRo1GgjMDEVL0VdPRvdktkIWVCK",
        'response' => $data['g-recaptcha-response'],
        'remoteip' => $_SERVER['REMOTE_ADDR']
      );
      $url = 'https://www.google.com/recaptcha/api/siteverify';
      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $captcha_data);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $response = curl_exec($ch);
      $result = json_decode($response, true);
      curl_close($ch);
      if ($result['success'] != true) {
        $arr_error[] = "Recaptcha Invalid, Please Try Again";
      }
    }

    if (count($arr_error) > 0) {
      echo json_encode(['is_success' => false, "message" => implode("\n", $arr_error)]);
      return false;
    }
    // END OF VALIDATION

    //uploading
    // uploads it under "Base path/assets" folder
    $imgSrc = "";

    if (!empty($imageFile['tmp_name'])) {
      $folderUpload = "Inquiries/" . date("Y-m-d");
      // . "/" . $data['email'] . "/";
      Folder::find_or_make($folderUpload);
      $file = File::create();
      $upload = Upload::create();

      $upload->loadIntoFile($imageFile, $file, $folderUpload);
      $imgSrc = $upload->getFile()->URL;
    }
    // end of uploading

    // echo "<pre>";
    // print_r($upload);
    // echo "</pre>";


    // email
    // php mailer
    $mail = new PHPMailer(true);
    $subject = $this->getSiteConfigTitle . " (Start Your Project Inquiry)";

    $body = "Name: " . $name . "<br>" .
      "Email: " . $email . "<br>" .
      "Phone: " . $phone . "<br>" .
      "Company Name: " . $companyName . "<br>" .
      "Message: " . $message;

    $mail->isSMTP();
    $mail->Host = "smtp.gmail.com";
    $mail->Port = 587;
    $mail->SMTPSecure = 'tls';
    $mail->SMTPAuth = true;

    $mail->Username =  "jerome@directworksmedia.com";
    $mail->Password = "092197092197";

    $mail->addReplyTo($data['email'], $name);
    $mail->setFrom("jerome@directworksmedia.com", $subject);

    // receiver
    $mail->addAddress("jerome@directworksmedia.com");

    $mail->Subject = $subject;
    $mail->Body = $body;

    //add attachment if upload file is present
    if (!empty($imageFile['tmp_name'])) {
      $mail->addAttachment(BASE_PATH . "/" . $imgSrc);
    }

    $mail->IsHTML(true);

    if (!$mail->Send()) {
      echo json_encode(['is_success' => false, "message" => $mail->ErrorInfo]);
    } else {

      // save db
      $saveDB = StartYourProjectInquiry::create([
        'FirstName' => $firstName,
        'LastName' => $lastName,
        'Email' => $email,
        'Phone' => $phone,
        'CompanyName' => $companyName,
        'Message' => $message,
        "ImgSrc" => $imgSrc
      ]);
      $saveDB->write();
      // end of save db

      echo json_encode(['is_success' => true]);
    }
    // end of php mailer

  }
}
