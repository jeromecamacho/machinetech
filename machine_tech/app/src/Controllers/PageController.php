<?php

namespace {

    use SilverStripe\SiteConfig\SiteConfig;


    use SilverStripe\Dev\Debug;
    use SilverStripe\Control\Director;
    use SilverStripe\ORM\FieldType\DBField;
    use SilverStripe\CMS\Controllers\ContentController;
    use SilverStripe\View\Requirements;

    include 'InquiryClass.php';


    class PageController extends ContentController
    {
        /**
         * An array of actions that can be accessed via a request. Each array element should be an action name, and the
         * permissions or conditions required to allow the user to access it.
         *
         * <code>
         * [
         *     'action', // anyone can access this action
         *     'action' => true, // same as above
         *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
         *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
         * ];
         * </code>
         *
         * @var array
         */
        private static $allowed_actions = [];

        public $getSiteConfigTitle;

        protected function init()
        {
            parent::init();
            // You can include any CSS or JS required by your project here.
            // See: https://docs.silverstripe.org/en/developer_guides/templates/requirements/

            // css
            Requirements::css("app/css/custom.css");
            Requirements::css("https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css");
            Requirements::css("https://unpkg.com/aos@2.3.1/dist/aos.css");
            Requirements::css("https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css");

            Requirements::css("app/css/intlTelInput.css");
            Requirements::customCSS(
                <<<CSS
                        .g-recaptcha {
                            display: inline-block;
                        }
                        .wrapper-recaptcha {
                            text-align: center;
                            margin-top: 27px;
                        }
                    CSS
            );


            // js
            Requirements::javascript("https://kit.fontawesome.com/7e408bb136.js");
            Requirements::javascript("https://code.jquery.com/jquery-3.4.1.min.js");
            Requirements::javascript("https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js");
            Requirements::javascript(
                "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            );
            Requirements::javascript("https://unpkg.com/aos@2.3.1/dist/aos.js");

            Requirements::javascript("https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js");
            Requirements::javascript("https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.js");
            Requirements::javascript("https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.js.map");
            Requirements::javascript("https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js.map");
            Requirements::javascript("app/js/file-drag-drop.js");
            Requirements::customScript(
                <<<JS
                    AOS.init();

                    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
                        event.preventDefault();
                        $(this).ekkoLightbox();
                    });

                    $(window).on('scroll', function(){
                        if($(window).scrollTop()){
                            $('.navbar-top').addClass('black');
                        }
                        else {
                            $('.navbar-top').removeClass('black');
                        }
                    })
                    
                JS
            );

            Requirements::javascript("https://www.google.com/recaptcha/api.js");
            Requirements::javascript("app/js/alphanum.js");
            Requirements::javascript("app/js/jquery.validate.js");
            Requirements::javascript("app/js/intlTelInput.js");
            Requirements::javascript("app/js/jquery.mask.js");

            $getSiteConfig = SiteConfig::current_site_config();
            $this->getSiteConfigTitle = $getSiteConfig->Title;
        }

        // ! GLOBAL FUNCTIONS
        public function getCleanBaseHref()
        {
            return str_replace("?stage=Stage", "", Director::absoluteBaseURL());
        }

        public function setRemovePTag($content)
        {
            // Get content from TinyMCE
            $content = preg_replace('/<p[^>]*>/', '', $content); // Remove the start <p> or <p attr="">
            $content = preg_replace('/<\/p>/', '', $content); // Replace the end

            return DBField::create_field(
                "HTMLText",
                $content
            );

            // return $content; // Output content without P tags
        }

        public function setPlainHTML($content)
        {
            // Get content from TinyMCE
            $content = preg_replace('/<p[^>]*>/', '', $content); // Remove the start <p> or <p attr="">
            $content = preg_replace('/<\/p>/', '<br>', $content); // Replace the end

            return DBField::create_field(
                "HTMLText",
                $content
            )->Plain();

            // return $content; // Output content without P tags
        }

        // Debug::show($faqList);

        // ! *************/
        // !    HELPERS
        // ! *************/
        public function _cleanString($data)
        {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        }

        public function _recaptchaResponse($urlGoogle)
        {
            $ch = @curl_init();
            @curl_setopt($ch, CURLOPT_POST, true);
            @curl_setopt($ch, CURLOPT_URL, $urlGoogle);
            @curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = @curl_exec($ch);
            $status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $curl_errors = curl_error($ch);
            @curl_close($ch);
            $decodedResponse = json_decode($response);
            return $decodedResponse;
        }

        public function getServicesPage()
        {
            return ServicesPage::get();
        }
        // ! END GLOBAL FUNCTIONS
    }
}
