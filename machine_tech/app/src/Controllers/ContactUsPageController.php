<?php

use SilverStripe\Control\Director;
use SilverStripe\View\Requirements;
use SilverStripe\Control\HTTPRequest;

class ContactUsPageController extends PageController
{

  private static $allowed_actions = [
    "_submitInquiry"
  ];

  private function getLinkAjax()
  {
    return str_replace("?stage=Stage", "", Director::get_current_page()->Link());
  }

  public function init()
  {
    parent::init();

    // ! FORMS SCRIPT
    $linkForAjax = $this->getLinkAjax();
    $cleanBaseHref = $this->getCleanBaseHref();
    $currentLink = $this->Link();

    Requirements::customScript(
      <<<JS
        $(document).ready(function () {

          const validateAll = () => {
            if ($("#idFrmInquiry").length) {
              // custom validation
              $.validator.addMethod("noSpace", (value, element) => {
                return value === '' || value.trim().length != 0
              }, "Spaces are not allowed");

              $.validator.addMethod("phoneValidation", (value, element) => {
                var isValid = true;
                if ($(element).hasClass('has-error')) {
                  isValid = false;
                }
                return isValid;
              }, "Invalid Phone Number");

              $('#idFrmInquiry').validate({
                // render error message corresponds on each fields
                // errorPlacement: function(error, element) {
                //   error.appendTo(element.closest("div"));
                // },

                // one div for all message
                errorContainer: $('div.container-error'),
                errorLabelContainer: $("ul", $('div.container-error')),
                wrapper: 'li',

                // to include the hidden field recaptcha for validation
                ignore: ".ignore",

                rules: {
                  first_name: {
                    required: true,
                    noSpace: true
                  },
                  last_name: {
                    required: true,
                    noSpace: true,
                  },
                  email: {
                    required: true,
                    email: true,
                    noSpace: true,
                  },
                  phone: {
                    required: true,
                    noSpace: true,
                    phoneValidation: true
                  },
                  company_name: {
                    required: true,
                    noSpace: true,
                  },
                  message: {
                    required: true,
                    noSpace: true
                  },
                  hidden_recaptcha: {
                    required: function () {
                      if (grecaptcha.getResponse() == '') {
                        return true;
                      } else {
                        return false;
                      }
                    }
                  }
                },
                // for customize the error message
                messages: {
                  first_name: {
                    required: "Please enter First Name"
                  },
                  last_name: {
                    required: "Please enter Last Name"
                  },
                  phone: {
                    required: "Please enter Phone Number"
                  },
                  email: {
                    required: "Please enter Email Address"
                  },
                  company_name: {
                    required: "Please enter Company Name"
                  },
                  message: {
                    required: "Please enter Message"
                  },
                  hidden_recaptcha: {
                    required: "Please check Recaptcha"
                  }
                }
              });
            }
          }

          validateAll();

          // for phone
          $('#phone').alphanum({
            allowLatin: false,
            allowOtherCharSets: false,
            allow: '+,()- ', // Allow extra characters
          });

          var input = document.querySelector("#phone");

          // initialise plugin
          var iti = window.intlTelInput(input, {
            // get current location of the user
            // initialCountry: "auto",
            // geoIpLookup: function(callback) {
            //     $.get('https://ipinfo.io', function() {}, "jsonp").always(function(resp) {
            //     var countryCode = (resp && resp.country) ? resp.country : "";
            //     callback(countryCode);
            //     });
            // },
            utilsScript: "app/js/utils.js"
          });

          var reset = function () {
            input.classList.remove("has-error");
          };

          // on blur: validate
          input.addEventListener('blur', function () {
            reset();
            if (input.value.trim()) {
              if (!iti.isValidNumber()) {
                input.classList.add("has-error");
              }
            }
          });

          // on keyup / change flag: reset
          input.addEventListener('change', reset);
          input.addEventListener('keyup', reset);
          // end of for phone

          $("#idFrmInquiry").on('submit', function (event) {

            var thisForm = $("#idFrmInquiry");
            var thisSubmitButton = $("#idFrmInquiry button");

            if (!$(thisForm).valid()) {
              return false;
            }
            event.preventDefault();

            var has_phone_error = 0;

            if ($("#phone").hasClass("has-error")) {
              has_phone_error = 1;
            }

            $.ajax({
              type: "POST",
              url: "$linkForAjax"+"_submitInquiry",
              beforeSend: function () {
                thisSubmitButton.html("<i class = 'fa fa-spinner fa-spin'></i> Please Wait...").attr("disabled", true);
              },
              data: thisForm.serialize() + "&has_phone_error=" + has_phone_error,
              success: function (response) {
                grecaptcha.reset();
                $("#idFrmInquiry")[0].reset();
                thisSubmitButton.html('Submit').attr("disabled", false);
                // console.log(response);

                var decodeResponse = JSON.parse(response);

                if (!decodeResponse.is_success) {
                  alert(decodeResponse.message);
                } else if (decodeResponse.is_success) {
                  window.location = "$currentLink"+"thank-you";
                } else {
                  alert(response);
                }

              }
            });

          });

        });
      JS
    );
  }

  // ! FORM SERVER SCRIPT
  public function _submitInquiry(HTTPRequest $request)
  {
    $data = $request->requestVars();

    $inquiry = new InquiryClass();
    $inquiry->secretRecaptcha = "6Leo9n8UAAAAADRo1GgjMDEVL0VdPRvdktkIWVCK";
    $inquiry->_setRequiredFieldsAndValidation(
      [
        'first_name' => $data['first_name'],
        'last_name' => $data['last_name'],
        'email' => $data['email'],
        'phone' => $data['phone'],
        'has_phone_error' => $data['has_phone_error'],
        'company_name' => $data['company_name'],
        'message' => $data['message'],
        'g-recaptcha-response' => $data['g-recaptcha-response']
      ]
    );
    $inquiry->_setEmailSettings(
      true,
      'smtp',
      0,
      'smtp.gmail.com',
      587,
      'tls',
      true,
      "jerome@directworksmedia.com",
      "092197092197",
      $data['email'],
      null,
      null,
      "jerome@directworksmedia.com",
      ["jerome@directworksmedia.com", "tisoy2197@gmail.com"],
      $this->getSiteConfigTitle . " (Contact Us Inquiry)",
      false,
      true
    );
    $inquiry->_goEmail(
      [
        'first_name' => $data['first_name'],
        'last_name' => $data['last_name'],
        'email' => $data['email'],
        'phone' => $data['phone'],
        'company_name' => $data['company_name'],
        'message' => $data['message']
      ]
    );
    $inquiry->_goDatabase(
      [
        'FirstName' => $data['first_name'],
        'LastName' => $data['last_name'],
        'Email' => $data['email'],
        'Phone' => $data['phone'],
        'CompanyName' => $data['company_name'],
        'Message' => $data['message']
      ],
      ContactInquiry::class
    );
  }
}
