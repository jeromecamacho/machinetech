<?php

use PHPMailer\PHPMailer\PHPMailer;

class InquiryClass
{
  public $secretRecaptcha;

  public $withEmail;

  public $emailIsSMTP;
  public $emailMailer;
  public $smtpDebug;
  public $emailHost;
  public $emailPort;
  public $emailSMTPSecure;
  public $emailSMTPAuth;
  public $emailSMTPEmail;
  public $emailAppPassword;
  public $emailAddReplyTo;
  public $emailReceiver;
  public $emailWithAttachment;
  public $emailAddBcc;
  public $emailAddCc;
  public $emailSetFrom;
  public $emailSubject;
  public $emailIsHTML;

  private $arrErrors = [];

  private $arrMessages = [];

  public function _setEmailSettings(
    $emailIsSMTP,
    $emailMailer,
    $smtpDebug,
    $emailHost,
    $emailPort,
    $emailSMTPSecure,
    $emailSMTPAuth,
    $emailSMTPEmail,
    $emailAppPassword,
    $emailAddReplyTo,
    $emailAddCc,
    $emailAddBcc,
    $emailSetFrom,
    $emailReceiver,
    $emailSubject,
    $emailWithAttachment,
    $emailIsHTML
  ) {
    $this->emailIsSMTP = $emailIsSMTP;
    $this->emailMailer = $emailMailer;
    $this->smtpDebug = $smtpDebug;
    $this->emailHost = $emailHost;
    $this->emailPort = $emailPort;
    $this->emailSMTPSecure = $emailSMTPSecure;
    $this->emailSMTPAuth = $emailSMTPAuth;
    $this->emailSMTPEmail = $emailSMTPEmail;
    // ? APP PASSWORD 
    // ? Notes: I generate App Password for its email client, cause it have multi-factor authentication
    // ? Reference : https://www.zoho.com/mail/help/zoho-smtp.html
    $this->emailAppPassword = $emailAppPassword;
    $this->emailAddReplyTo = $emailAddReplyTo;
    $this->emailAddCc = $emailAddCc;
    $this->emailAddBcc = $emailAddBcc;
    $this->emailSetFrom = $emailSetFrom;
    $this->emailReceiver = $emailReceiver;
    $this->emailSubject = $emailSubject;
    $this->emailWithAttachment = $emailWithAttachment;
    $this->emailIsHTML = $emailIsHTML;
  }

  // * @param array
  public function _goEmail($arrFields)
  {

    $arrEmailBody = [];
    foreach ($arrFields as $key_arrFields => $value_arrFields) {
      if (trim($value_arrFields) != "") {
        //check if 2 or more characters, insert space if have
        $expString = explode("_", $key_arrFields);
        $fieldsNames = [];
        if (count($expString) >= 2) {
          foreach ($expString as $each_expString) {
            $fieldsNames[] = ucfirst($each_expString);
          }
        } else {
          $fieldsNames[] = ucfirst($expString[0]);
        }

        $arrEmailBody[] = implode(" ", $fieldsNames) . " : " . $value_arrFields;
      }
    }
    $emailBody = implode("<br>", $arrEmailBody);

    $mail = new PHPMailer(true);
    if ($this->emailIsSMTP) {
      $mail->isSMTP();
    }
    $mail->Mailer = $this->emailMailer ?? 'smtp';
    $mail->SMTPDebug = $this->smtpDebug ?? 0;
    $mail->Host = $this->emailHost ?? "smtp.gmail.com";
    $mail->Port = $this->emailPort ?? 587;
    $mail->SMTPSecure = $this->emailSMTPSecure ?? 'tls';
    $mail->SMTPAuth = $this->emailSMTPAuth ?? true;

    $mail->Username = $this->emailSMTPEmail;
    $mail->Password = $this->emailAppPassword;

    foreach ($this->emailReceiver as $eachEmailReceiver) {
      $mail->addAddress($eachEmailReceiver);
    }

    if (!is_null($this->emailAddReplyTo)) {
      $mail->addReplyTo($this->emailAddReplyTo);
    }


    if (!is_null($this->emailAddCc)) {
      $mail->addCC($this->emailAddCc);
    }
    if (!is_null($this->emailAddBcc)) {
      $mail->addBCC($this->emailAddBcc);
    }
    $mail->setFrom($this->emailSetFrom, $this->emailSubject);

    $mail->Subject = $this->emailSubject;
    $mail->Body = $emailBody;

    if ($this->emailWithAttachment) {
      // $mail->addAttachment(BASE_PATH.'/pdf/Exclusive-Lifestyle-Program.pdf');
    }

    $mail->isHTML($this->emailIsHTML);

    if (!$mail->Send()) {
      $this->arrMessages[] = $mail->ErrorInfo;
    }

    // $this->_getFinalResult();
  }

  public function _goDatabase($arrFields, $dataObjectName)
  {
    $saveDBInquiry = $dataObjectName::create();
    foreach ($arrFields as $key_arrFields => $value_arrFields) {
      $saveDBInquiry->$key_arrFields = $value_arrFields;
    }
    $saveDBInquiry->write();

    if (empty($saveDBInquiry->ID)) {
      $this->arrMessages[] = "Database save error";
    }

    $this->_getFinalResult();
  }

  public function _getFinalResult()
  {
    if (count($this->arrMessages)) {
      echo json_encode(array("is_success" => false, "message" => implode("\n", $this->arrMessages)));
    } else {
      echo json_encode(array("is_success" => true));
    }
  }

  // * @param array
  public function _setRequiredFieldsAndValidation($arrFields)
  {

    if (!is_array($arrFields)) {
      $arrFields = [$arrFields];
    }

    //required fields validation
    foreach ($arrFields as $arrFieldKey => $arrFieldValue) {
      if (trim($arrFieldValue) == "" || !isset($arrFieldValue)) {

        //check if 2 or more characters, insert space if have
        $expString = explode("_", $arrFieldKey);
        $fieldsNames = [];
        if (count($expString) >= 2) {
          foreach ($expString as $each_expString) {
            $fieldsNames[] = ucfirst($each_expString);
          }
        } else {
          $fieldsNames[] = ucfirst($expString[0]);
        }

        $this->arrErrors[] = implode(" ", $fieldsNames) . " is required.";
      }
    }

    //added validation
    if (array_key_exists("phone", $arrFields) && array_key_exists("has_phone_error", $arrFields)) {
      if (!empty(trim($arrFields['phone'])) && $arrFields['has_phone_error'] == 1) {
        $this->arrErrors[] = "Invalid Phone Number";
      }
    }

    if (array_key_exists("email", $arrFields)) {
      if (!filter_var(trim($arrFields['email']), FILTER_VALIDATE_EMAIL) && !empty(trim($arrFields['email']))) {
        $this->arrErrors[] = "Email is Invalid";
      }
    }

    if (array_key_exists("age", $arrFields)) {
      if (!is_int(intval($arrFields['age']))) {
        $this->arrErrors[] = "Age is Invalid";
      }
    }

    if (array_key_exists("number_of_guest", $arrFields)) {
      if (!is_int(intval($arrFields['number_of_guest']))) {
        $this->arrErrors[] = "Number Of Guest is Invalid";
      }
    }

    if (array_key_exists("g-recaptcha-response", $arrFields)) {
      if (trim($arrFields['g-recaptcha-response']) === "") {
        $this->arrErrors[] = "Please check the captcha form";
      } else {
        $captcha_data = array(
          'secret' => $this->secretRecaptcha,
          'response' => $arrFields['g-recaptcha-response'],
          'remoteip' => $_SERVER['REMOTE_ADDR']
        );
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $captcha_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        $result = json_decode($response, true);
        curl_close($ch);
        if ($result['success'] != true) {
          $this->arrErrors[] = "Recaptcha Invalid, Please Try Again";
        }
      }
    }

    if (count($this->arrErrors)) {
      echo json_encode(['is_success' => false, "message" => implode("\n", $this->arrErrors)]);
    }
  }
}
