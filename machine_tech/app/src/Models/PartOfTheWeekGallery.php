<?php

use SilverStripe\Assets\Image;
use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use Bummzack\SortableFile\Forms\SortableUploadField;

class PartOfTheWeekGallery extends DataObject
{
  private static $singular_name = "Gallery";
  private static $default_sort = "Sort ASC";

  private static $db = [
    "Sort" => "Int",
    "ContentLabel" => "Text"
  ];

  private static $has_one = [
    'PartOfTheWeekPage' => PartOfTheWeekPage::class
  ];

  // This page can have many images
  private static $many_many = [
    'Images' => Image::class
  ];

  // this adds the SortOrder field to the relation table.
  // Please note that the key (in this case 'Images')
  // has to be the same key as in the $many_many definition!
  private static $many_many_extraFields = [
    'Images' => ['SortOrder' => 'Int']
  ];

  private static $searchable_fields = [
    "ContentLabel"
  ];

  private static $summary_fields = [
    "ID" => "#",
    "ContentLabel" => "Title",
  ];

  public function getCMSFields()
  {

    $fields = FieldList::create(
      TextField::create("ContentLabel", "Title"),
      $Images = SortableUploadField::create("Images", $this->fieldLabel('Images'))->setSortColumn('SortOrder'),
    );

    $Images->setAllowedExtensions(['png', 'gif', 'jpg', 'jpeg']);
    $Images->setFolderName('PartOfTheWeek-Page-Image');

    $fields->removeByName('Sort');
    return $fields;
  }


  // Use this in your templates to get the correctly sorted images
  public function getSortedImages()
  {
    return $this->Images()->Sort('SortOrder');
  }


  protected function onBeforeWrite()
  {
    if (!$this->Sort || $this->Sort == 0) {
      $this->Sort = PartOfTheWeekGallery::get()->max('Sort') + 1;
    }
    parent::onBeforeWrite();
  }
}
