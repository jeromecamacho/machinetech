<?php

use SilverStripe\Assets\File;
use SilverStripe\ORM\DataObject;
use SilverStripe\ORM\FieldType\DBField;

class StartYourProjectInquiry extends DataObject
{
  private static $singular_name = "Start Your Project Inquiry";
  private static $default_sort = 'ID DESC';

  private static $db = [
    "FirstName" => "Text",
    "LastName" => "Text",
    "Email" => "Text",
    "Phone" => "Text",
    "CompanyName" => "Text",
    "Message" => "Text",
    "ImgSrc" => "Text"
  ];

  private static $searchable_fields = [
    "FirstName",
    "LastName",
    "Email",
    "Phone",
    "CompanyName",
    "Message"
  ];

  // private static $has_one = [
  //   "ImgSrc" => File::class
  // ];

  private static $summary_fields = [
    "DateFormatted" => "Date Created",
    "FirstName" => "First Name",
    "LastName" => "Last Name",
    "Email",
    "Phone",
    "CompanyName" => "Company Name",
    "Message",
    "PreviewAttachment" => "Attachment"
  ];

  //*****************
  // custom functions
  //*****************
  public function getDateFormatted()
  {
    // return $this->dbObject('Created')->format('F d, Y H:i A');
    return $this->dbObject('Created')->format('MMM d, Y');
  }

  public function getPreviewAttachment()
  {
    if ($this->ImgSrc) {
      return DBField::create_field('HTMLText', '
      <a style = "margin-top:10px;" href = "' . $this->ImgSrc . '" target = "_blank"><i class = "font-icon-export"></i> Large View</a>
      <br>
      <img src="' . $this->ImgSrc . '"
      style = "
          border: 1px solid #ddd; /* Gray border */
          border-radius: 4px;  /* Rounded border */
          padding: 5px; /* Some padding */
          width: 150px; /* Set a small width */
          height: 100px;
          "
      />
      
      ');
    }
    return "No Attachment Uploaded";
  }
}
