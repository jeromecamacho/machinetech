<?php

use SilverStripe\ORM\DataObject;

class ContactInquiry extends DataObject
{
  private static $singular_name = "Contact Inquiry";
  private static $default_sort = 'ID DESC';

  private static $db = [
    "FirstName" => "Text",
    "LastName" => "Text",
    "Email" => "Text",
    "Phone" => "Text",
    "CompanyName" => "Text",
    "Message" => "Text",
  ];

  private static $searchable_fields = [
    "FirstName",
    "LastName",
    "Email",
    "Phone",
    "CompanyName",
    "Message"
  ];

  private static $summary_fields = [
    "DateFormatted" => "Date Created",
    "FirstName" => "First Name",
    "LastName" => "Last Name",
    "Email",
    "Phone",
    "CompanyName" => "Company Name",
    "Message"
  ];

  //*****************
  // custom functions
  //*****************
  public function getDateFormatted()
  {
    // return $this->dbObject('Created')->format('F d, Y H:i A');
    return $this->dbObject('Created')->format('MMM d, Y');
  }
}
