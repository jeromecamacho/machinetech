<?php

use SilverStripe\Forms\DateField;
use SilverStripe\Admin\ModelAdmin;
use SilverStripe\Forms\GridField\GridFieldConfig;
use Tedy\GridFieldCustom\GridFieldDeleteAllButton;
use SilverStripe\Forms\GridField\GridFieldPageCount;
use SilverStripe\Forms\GridField\GridFieldPaginator;
use Tedy\GridFieldCustom\GridFieldMultiDeleteButton;
use SilverStripe\Forms\GridField\GridFieldEditButton;
use SilverStripe\Forms\GridField\GridFieldAddNewButton;
use SilverStripe\Forms\GridField\GridFieldImportButton;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;
use Tedy\GridFieldCustom\GridFieldCheckboxSelectComponent;

class InquiryAdmin extends ModelAdmin
{

  private static $managed_models = [
    ContactInquiry::class,
    StartYourProjectInquiry::class
  ];

  private static $url_segment = 'inquiry';
  private static $menu_title = 'Inquiries';
  private static $menu_icon_class = 'font-icon-chat';

  protected function getGridFieldConfig(): GridFieldConfig
  {
    $config = parent::getGridFieldConfig();

    $config->removeComponentsByType(GridFieldAddNewButton::class);
    $config->removeComponentsByType(GridFieldEditButton::class);

    $config->addComponent(
      new GridFieldCheckboxSelectComponent()
    );
    // buttons-before-right
    // -position right side before the search, it will not hide the buttons upon search
    $config->addComponent(
      new GridFieldMultiDeleteButton('before')
    );
    $config->addComponent(
      new GridFieldDeleteAllButton('before')
    );

    // $config->addComponent(new GridFieldOrderableRows());
    $config->removeComponentsByType(GridFieldImportButton::class);

    // $config->removeComponentsByType(GridFieldPaginator::class);
    // $config->removeComponentsByType(GridFieldPageCount::class);

    return $config;
  }

  /**
   * Returns GridField filter params
   */
  public function getFilterParams()
  {
    $class = $this->sanitiseClassName($this->modelClass);
    $body  = $this->getRequest()->getBody();

    parse_str($body, $array);

    $params = [];

    // first, try to get params from filter state in request body
    if (isset($array['filter'][$class]) && is_array($array['filter'][$class])) {
      $params = $array['filter'][$class];
    }

    // second, try to get params from GridFieldFilterHeader state in request body
    if (empty($params) && isset($array[$class]['GridState']) && $array[$class]['GridState']) {
      $gridState = json_decode($array[$class]['GridState'], true);

      if (
        isset($gridState['GridFieldFilterHeader']['Columns'])
        && is_array($gridState['GridFieldFilterHeader']['Columns'])
      ) {
        $params = $gridState['GridFieldFilterHeader']['Columns'];
      }
    }

    // third, try to get params from GridFieldFilterHeader state in request vars
    if (empty($params) && $requestVar = $this->getRequest()->requestVar($class)) {
      if (isset($requestVar['GridState']) && !empty($requestVar['GridState'])) {
        $gridState = json_decode($requestVar['GridState'], true);

        if (
          isset($gridState['GridFieldFilterHeader']['Columns'])
          && is_array($gridState['GridFieldFilterHeader']['Columns'])
        ) {
          $params = $gridState['GridFieldFilterHeader']['Columns'];
        }
      }
    }
    return $params;
  }

  public function getList()
  {
    // get list
    $list = parent::getList();

    // get search params
    $params = $this->getFilterParams();

    if (isset($params['Created'])) {
      // do filter stuffs
      $list->filter('Created', $params['Created']);
    }

    return $list;
  }
}
