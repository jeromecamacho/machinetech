<section class="footer">
		<div class="container">
				<div class="row mb-5 align-items-center">
						<div class="col-6">
								<a href="{$getCleanBaseHref}"><img src="{$SiteConfig.FooterLogo.URL}" alt = "No Image Specified"/></a>
						</div>
						<div class="col-6 text-right text-md-left">
								<div class="underlined pl-0 pl-md-4">
									<% with $SiteConfig %>
										<h1 class="pb-1">{$TeaserLabel1}</h1>
										<p>{$TeaserLabel2}</p>
									<% end_with %>
								</div>
						</div>
				</div>
				<div class="row">
						<div class="col-md-12 col-lg-3 mb-3 mb-lg-0 order-first">
								<h4>Contact</h4>
								<% with $SiteConfig %>
								<ul class="list-unstyled">
										<% if $Location %>
										<li><a href="{$Location.LinkURL}" {$Location.TargetAttr}><i class="fas fa-map-marker-alt mr-2"></i> {$Location.Title}</a></li>
										<% end_if %>
										<li><a href="tel:{$Phone}"><i class="fas fa-phone-alt mr-2"></i>{$Phone}</a></li>
										<li><a href="mailto:{$Email}"><i class="fas fa-envelope mr-2"></i>{$Email}</a></li>
										<% if $FacebookLink %>
										<li><a href="{$FacebookLink.LinkURL}" {$FacebookLink.TargetAttr}><i class="fab fa-facebook-f mr-2"></i>$FacebookLink.Title</a></li>
										<% end_if %>
								</ul>
								<% end_with %>
						</div>
						<div class="col-6 col-lg order-4 order-lg-2">
								<h4>About</h4>
								<ul class="list-unstyled">
									<% loop $MenuSet('Footer About').MenuItems %>
										<li><a href="{$Link}">{$MenuTitle}</a></li>
									<% end_loop %>
								</ul>
						</div>
						<div class="col-6 col-lg mb-4 mb-lg-0 order-2 order-lg-3">
								<h4>Services</h4>
								<ul class="list-unstyled">
									<% loop $MenuSet('Footer Services 1').MenuItems %>
										<li><a href="{$Link}">{$MenuTitle}</a></li>
									<% end_loop %>
								</ul>
						</div>
						<div class="col-6 col-lg order-3 order-lg-4">
								<h4>&nbsp;</h4>
								<ul class="list-unstyled">
									<% loop $MenuSet('Footer Services 2').MenuItems %>
										<li><a href="{$Link}">{$MenuTitle}</a></li>
									<% end_loop %>
								</ul>
						</div>
						<div class="col-6 col-lg order-last">
								<h4>&nbsp;</h4>
								<ul class="list-unstyled">
									<% loop $MenuSet('Footer Other').MenuItems %>
										<li><a href="{$Link}">{$MenuTitle}</a></li>
									<% end_loop %>
								</ul>
						</div>
				</div>
		</div>
</section>
<footer-2>
		<div class="text-center py-2">
				{$SiteConfig.Copyright} 
		</div>
</footer-2>
<!--- DWM FOOTER --->
<div style="background-color:#1B1B1B;color:#fff;padding:10px 0px;font-size:13px;-webkit-font-smoothing: antialiased;letter-spacing: 0.02em;font-family: Open Sans, Arial, Serif;">
		<div class="container">
				<div class="row">
						<div class="col text-center">
								Powered by <a href="http://www.directworksmedia.com/" target="_blank"><img src="app/img/dwm-logo-black.png" style="vertical-align:middle"></a>
						</div>
				</div>
		</div>
</div>
<!--- / DWM FOOTER --->