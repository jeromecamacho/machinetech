<?php

use SilverStripe\Assets\File;
use SilverStripe\Admin\CMSMenu;
use SilverStripe\Admin\LeftAndMain;
use SilverStripe\Security\Member;
use SilverStripe\View\Requirements;
use SilverStripe\SiteConfig\SiteConfig;
use SilverStripe\Security\PasswordValidator;
use SilverStripe\CampaignAdmin\CampaignAdmin;
use SilverStripe\Forms\HTMLEditor\TinyMCEConfig;
use SilverStripe\Forms\HTMLEditor\HTMLEditorConfig;

// remove PasswordValidator for SilverStripe 5.0
$validator = PasswordValidator::create();
// Settings are registered via Injector configuration - see passwords.yml in framework
Member::set_password_validator($validator);


// CUSTOM CONFIG

//WYSIWYG Config
HTMLEditorConfig::get('cms')->setOption(
  'extended_valid_elements',
  '*[*]'
);
HTMLEditorConfig::get('cms')->setOption(
  'valid_elements',
  '*[*]'
);
HTMLEditorConfig::get('cms')->setOption(
  'valid_children',
  '*[*]'
);

TinyMCEConfig::get('cms')
  ->addButtonsToLine(1, 'strikethrough')
  ->addButtonsToLine(2, 'styleselect')
  ->setOptions([
    'importcss_append', true,
  ]);


TinyMCEConfig::get('cms')->insertButtonsBefore('formatselect', 'fontsizeselect');
TinyMCEConfig::get('cms')->setOptions([
  'fontsize_formats' => "1px 2px 3px 4px 5px 6px 7px 8px 9px 10px 11px 12px 13px 14px 15px 16px 17px 18px 19px 20px 21px 22px 23px 24px 25px 26px 27px 28px 29px 30px 31px 32px 33px 34px 35px 36px 37px 38px 39px 40px 41px 42px 43px 44px 45px 46px 47px 48px 49px 50px 51px 52px 53px 54px 55px 56px 57px 58px 59px 60px 61px 62px 63px 64px 65px 66px 67px 68px 69px 70px 71px 72px 73px 74px 75px 76px 77px 78px 79px 80px 81px 82px 83px 84px 85px 86px 87px 88px 89px 90px 91px 92px 93px 94px 95px 96px 97px 98px 99px 100px"
]);

CMSMenu::remove_menu_class(CampaignAdmin::class);
// Requirements::block("silverstripe/login-forms: client/dist/styles/darkmode.css");

//extensions
SiteConfig::add_extension(SiteConfigExtension::class);
File::add_extension(FileExtension::class);
LeftAndMain::add_extension(LeftAndMainExtension::class);
InquiryAdmin::add_extension(InquiryAdminExtension::class);

// sample changing background color of htmleditorfield
$homeHeroConfig = clone TinyMCEConfig::get('cms');
$homeHeroConfig->setOption('body_class', 'typography typography--home-hero typography--green-background');
TinyMCEConfig::set_config('home_hero', $homeHeroConfig);
// END OF CUSTOM CONFIG